// SPDX-License-Identifier: CC0-1.0
use anyhow::{anyhow, Context, Result};
use chrono::{DateTime, Datelike};
use clap::Parser;
use regex::Regex;
use reqwest::header::{CONTENT_DISPOSITION, LAST_MODIFIED};
use reqwest::Client;
use std::collections::HashSet;
use std::path::{Path, PathBuf};
use tokio::fs::{self, File};
use tokio::io::AsyncWriteExt;

const MAP_INDEX_URL: &str = "https://new.mta.info/maps";

/// MTA Map Fetcher
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Cli {
    /// Run the operation in dry run mode without downloading or deleting files.
    #[arg(long)]
    dry_run: bool,

    /// Directory to download the files to.
    #[arg(long, default_value = ".")]
    download_dir: PathBuf,

    /// Skip deleting old maps
    #[arg(long)]
    skip_delete: bool,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    let args = Cli::parse();

    let client = Client::new();

    let urls = fetch_urls(&client).await?;
    let mut downloaded_files = HashSet::new();

    for url in &urls {
        download_file(
            &client,
            url,
            &args.download_dir,
            args.dry_run,
            &mut downloaded_files,
        )
        .await?;
    }

    if !args.skip_delete {
        clean_up_files(&args.download_dir, &downloaded_files, args.dry_run)
            .await?;
    }

    Ok(())
}

async fn fetch_urls(client: &Client) -> Result<Vec<String>> {
    let body = client
        .get(MAP_INDEX_URL)
        .send()
        .await?
        .text()
        .await
        .context("Failed to fetch the map index")?;

    let re = Regex::new(r#"/map/\d+"#).expect("Failed to compile regex");
    let mut urls: Vec<String> = vec![];

    for cap in re.captures_iter(&body) {
        let link = format!("https://new.mta.info{}", &cap[0]);
        urls.push(link);
    }

    Ok(urls)
}

async fn download_file(
    client: &Client,
    url: &str,
    download_dir: &Path,
    dry_run: bool,
    downloaded_files: &mut HashSet<PathBuf>,
) -> Result<()> {
    println!("Downloading {}", url);
    let response = client
        .get(url)
        .send()
        .await
        .context(format!("Failed to fetch the link: {}", url))?;

    let content_disposition = response
        .headers()
        .get(CONTENT_DISPOSITION)
        .and_then(|v| v.to_str().ok())
        .ok_or(anyhow!("Content-Disposition header not found"))?;

    let last_modified = response
        .headers()
        .get(LAST_MODIFIED)
        .and_then(|v| v.to_str().ok())
        .ok_or(anyhow!("Last-Modified header not found"))?;

    let filename = content_disposition
        .split(';')
        .find(|&part| part.trim_start().starts_with("filename="))
        .and_then(|part| part.split('=').nth(1))
        .map(|name| name.trim_matches('"').replace(' ', "_").replace(':', ""))
        .context("Filename not found in content disposition")?;

    let date = last_modified
        .split(',')
        .nth(1)
        .map(|s| s.trim())
        .context("Invalid last modified format")?;
    let date = DateTime::parse_from_rfc2822(date)
        .context(format!("Failed to parse last-modified date: {date}"))?;

    let file_path = if date.year() == 1970 {
        // Skip adding timestamp if the date is from 1970 (UNIX epoch most likely)
        download_dir.join(filename)
    } else {
        let date_str = date.format("%Y-%m-%d").to_string();
        // Remove the .pdf extension, append the date, and re-add the .pdf extension
        let base_filename = filename.trim_end_matches(".pdf");
        download_dir.join(format!("{}_{}.pdf", base_filename, date_str))
    };

    // Check if the file already exists
    if file_path.exists() {
        println!("File already exists: {:?}", file_path);
        downloaded_files.insert(file_path.clone());
        return Ok(());
    }

    if dry_run {
        println!("Would download and save to: {:?}", file_path);
    } else {
        let mut file = File::create(&file_path)
            .await
            .context(format!("Failed to create file: {:?}", file_path))?;
        file.write_all(&response.bytes().await?)
            .await
            .context(format!("Failed to write to file: {:?}", file_path))?;
    }
    downloaded_files.insert(file_path);

    Ok(())
}

async fn clean_up_files(
    download_dir: &Path,
    downloaded_files: &HashSet<PathBuf>,
    dry_run: bool,
) -> Result<()> {
    let mut dir = fs::read_dir(download_dir).await?;
    while let Some(entry) = dir.next_entry().await? {
        let path = entry.path();
        let filename = path
            .file_name()
            .and_then(|name| name.to_str())
            .unwrap_or("");

        if path.extension().and_then(|ext| ext.to_str()) == Some("pdf")
            && !downloaded_files
                .iter()
                .any(|downloaded_file| downloaded_file.ends_with(filename))
        {
            if dry_run {
                println!("Would delete file: {:?}", path);
            } else {
                println!("Deleting file: {:?}", path);
                fs::remove_file(&path)
                    .await
                    .context(format!("Failed to delete file: {:?}", path))?;
            }
        }
    }

    Ok(())
}
