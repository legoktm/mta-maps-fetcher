## 0.1.1 / 2024-05-18
* Remove ":" character from filenames for better filesystem compatibility.
* Fix typo of project name in documentation

## 0.1.0 / 2024-05-18

* Initial release.
