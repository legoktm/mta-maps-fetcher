# MTA Maps Fetcher

## Overview

The MTA Maps Fetcher is a command-line tool designed to download PDF map files from the MTA website. The tool fetches links to maps, downloads them, and saves them to a specified directory. It also includes options for dry run mode, specifying the download directory, and skipping the deletion of old files.

The intention is to set this up on a timer (see `contrib/` for sample systemd units) and use a file
synchronization service (e.g. [syncthing](https://syncthing.net/)) to copy to your phone for both offline and regularly-updated maps.

## Installation

Ensure you have Rust installed on your system, and then:

```sh
cargo install mta-map-fetcher
```

Pre-built binaries may be offered in the future.

## Usage

Run the tool with the desired options:

```sh
MTA Map Fetcher

Usage: mta-map-fetcher [OPTIONS]

Options:
      --dry-run                      Run the operation in dry run mode without downloading or deleting files
      --download-dir <DOWNLOAD_DIR>  Directory to download the files to [default: .]
      --skip-delete                  Skip deleting old maps
  -h, --help                         Print help
  -V, --version                      Print version
```

## License

This project is released into the public domain. Most of it was written by ChatGPT 4o and therefore
ineligible for copyright protection.
